<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'DashboardController')->name('dashboard');
Route::resource('orders', 'OrderController')->only('index');
Route::resource('products', 'ProductController')->only('index', 'create', 'edit', 'destroy');

Route::resource('photos', 'PhotoController')->only('store');
Route::get('settings', 'SettingController')->name('settings');
Route::get('texts', 'TextController')->name('texts');
