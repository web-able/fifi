module.exports = {
    theme: {
        extend: {
            spacing: {
                '60vh': '60vh',
                '74vh': '74vh',
            }
        },
    },
    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },
    plugins: []
}
