<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeFeatured($query)
    {
        return $query->whereIsFeatured(true);
    }
}
