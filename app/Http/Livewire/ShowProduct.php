<?php

namespace App\Http\Livewire;

use Auth;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\ { Product, Order, User};

class ShowProduct extends Component
{
    public $product;
    public $phone;
    public $address;
    public $additions;
    public $success = false;
    public $user;

    public function mount(Product $product)
    {
        $this->product = $product;
        $this->user = Auth::user();

        if ($this->user) {
            $this->fill(Arr::only($this->user->getAttributes(), ['phone', 'address', 'additions']));
        }
    }

    public function order()
    {
        $validated = $this->validate([
            'phone' => 'required|string|min:10|max:20',
            'address' => 'required|string|min:10|max:255',
            'additions' => 'nullable|string|max:255',
        ]);

        Order::create($validated + [
            'product_id' => $this->product->id,
            'status' => 'Обработва се',
        ]);

        if ($this->user) {
            $this->user->update($validated);
        } else {
            $this->user = User::create($validated + [
                'email' => Str::random().time().'@fifi.fifi',
                'password' => Str::random(),
                'is_admin' => false,
            ]);
        }

        Auth::login($this->user, $remember = true);

        $this->success = true;
    }
}
