<?php

namespace App\Http\Livewire;

use App\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ShowProducts extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.show-products', [
            'products' => Product::latest()->paginate(12),
        ]);
    }
}
