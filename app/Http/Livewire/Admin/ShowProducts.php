<?php

namespace App\Http\Livewire\Admin;

use Cache;
use App\Order;
use App\Product;
use Livewire\Component;

class ShowProducts extends Component
{
    public $products;
    public $hasDeletionFailed = false;

    public function mount()
    {
        $this->products = Product::latest()->get();
    }
    
    public function delete(int $id)
    {
        if (Order::whereProductId($id)->exists()) {
            return $this->hasDeletionFailed = true;
        }

        $product = Product::find($id);

        if ($product->is_featured) {
            Cache::forget('featured-products');
        }

        $product->delete();

        $this->products = Product::latest()->get();
    }
}
