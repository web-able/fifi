<?php

namespace App\Http\Livewire\Admin;

use DB;
use Cache;
use Livewire\Component;
use Illuminate\Support\Arr;

class SaveTexts extends Component
{
    public $products_text;
    public $landing_hero_title;
    public $landing_hero_description;
    public $landing_hero_c2a;
    public $landing_quality_title;
    public $landing_quality_description;
    public $landing_delivery_title;
    public $landing_delivery_description;
    public $landing_payment_title;
    public $landing_payment_description;
    public $page_delivery;
    public $page_personal_data;
    public $page_terms;

    public function mount()
    {
        $this->fill(Arr::only(settings()->getAttributes(), [
            'products_text',
            'landing_hero_title',
            'landing_hero_description',
            'landing_hero_c2a',
            'landing_quality_title',
            'landing_quality_description',
            'landing_delivery_title',
            'landing_delivery_description',
            'landing_payment_title',
            'landing_payment_description',
            'page_delivery',
            'page_personal_data',
            'page_terms',
        ]));
    }

    public function save()
    {
        $validated = $this->validate([
            'products_text' => 'required|string|max:40',
            'landing_hero_title' => 'required|string|max:40',
            'landing_hero_description' => 'required|string|max:80',
            'landing_hero_c2a' => 'required|string|max:40',
            'landing_quality_title' => 'required|string|max:40',
            'landing_quality_description' => 'required|string|max:255',
            'landing_delivery_title' => 'required|string|max:40',
            'landing_delivery_description' => 'required|string|max:255',
            'landing_payment_title' => 'required|string|max:40',
            'landing_payment_description' => 'required|string|max:255',
            'page_delivery' => 'required|string|max:65000',
            'page_personal_data' => 'required|string|max:65000',
            'page_terms' => 'required|string|max:65000',
        ]);

        DB::table('settings')->update($validated);

        Cache::forget('settings');

        return redirect()->route('admin.texts');
    }
}
