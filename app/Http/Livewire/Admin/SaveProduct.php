<?php

namespace App\Http\Livewire\Admin;

use Cache;
use App\Product;
use Livewire\Component;
use Illuminate\Support\Arr;

class SaveProduct extends Component
{
    public $productId;
    public $photo;
    public $title;
    public $price;
    public $description;
    public $isFeatured;

    public function mount(?Product $product = null)
    {
        $this->fill([
            'isFeatured' => $product->is_featured,
            'productId' => $product->id,
        ] + Arr::only($product->getAttributes(), [
            'productId', 'photo', 'title', 'price', 'description'
        ]));
    }

    public function save()
    {
        $validated = $this->validate([
            'photo' => 'required|uploaded',
            'title' => 'required|string|max:40',
            'price' => 'required|numeric|between:1,999999',
            'description' => 'required|string',
            'isFeatured' => 'nullable|bool',
        ]);

        $validated = [
            'is_featured' => $validated['isFeatured'] ?? false
        ] + Arr::except($validated, 'isFeatured');

        $this->productId ? Product::where('id', $this->productId)->update($validated) : Product::create($validated);

        Cache::forget('featured-products');

        return redirect()->route('admin.products.index');
    }
}
