<?php

namespace App\Http\Livewire\Admin;

use DB;
use Cache;
use Livewire\Component;
use Illuminate\Support\Arr;

class SaveSettings extends Component
{
    public $name;
    public $color;

    public function mount()
    {
        $this->fill(Arr::only(settings()->getAttributes(), ['name', 'color']));
    }

    public function save()
    {
        $validated = $this->validate([
            'name' => 'required|string|max:40',
            'color' => 'required|string|in:red,green,orange,blue,purple,pink',
        ]);

        DB::table('settings')->update($validated);

        Cache::forget('settings');

        return redirect()->route('admin.settings');
    }
}
