<?php

namespace App\Http\Livewire\Admin;

use App\Order;
use Livewire\Component;

class ShowOrders extends Component
{
    public $orders;

    public function mount()
    {
        $this->refreshOrders();
    }

    public function render()
    {
        return view('livewire.admin.show-orders');
    }

    public function refreshOrders()
    {
        $this->orders = Order::latest()->with('product')->get();
    }

    public function changeStatus(int $id, string $status)
    {
        Order::where('id', $id)->update(compact('status'));

        $this->refreshOrders();
    }
}
