<?php

namespace App\Http\Livewire\Admin;

use App\Mail;
use App\User;
use Livewire\Component;

class Contact extends Component
{
    public $title;
    public $description;
    public $success = false;
    public $email;

    public function mount()
    {
        $this->email = User::whereIsAdmin(true)->value('email');
    }

    public function send()
    {
        $validated = $this->validate([
            'title' => 'required|string|max:40',
            'description' => 'required|string|max:65000',
        ]);

        Mail::create($validated + ['from' => $this->email]);

        $this->success = true;
    }
}
