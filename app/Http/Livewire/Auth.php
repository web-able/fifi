<?php

namespace App\Http\Livewire;

use Auth as AuthFacade;
use Livewire\Component;

class Auth extends Component
{
    public $email;
    public $password;
    public $hasErrors = false;

    public function render()
    {
        return view('livewire.auth');
    }

    public function auth()
    {
        $this->reset('hasErrors');

        $credentials = [
            'email' => $this->email,
            'password' => $this->password,
            'is_admin' => true,
        ];

        if (AuthFacade::attempt($credentials, $remember = true)) {
            return redirect()->intended('dashboard');
        }

        $this->hasErrors = true;
    }
}
