<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show(string $slug)
    {
        return view('pages.show', compact('slug'));
    }
}
