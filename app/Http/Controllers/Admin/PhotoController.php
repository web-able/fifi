<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    public function store()
    {
        return request()->filepond->store('/');
    }
}
