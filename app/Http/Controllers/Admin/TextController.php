<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class TextController extends Controller
{
    public function __invoke()
    {
        return view('admin.texts');
    }
}
