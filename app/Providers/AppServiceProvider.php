<?php

namespace App\Providers;

use Storage;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('bg');

        Model::unguard();

        Validator::extend('uploaded', function ($attribute, $value, $parameters, $validator) {
            return Storage::exists($value);
        });
    }
}
