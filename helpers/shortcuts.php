<?php

use App\{ Setting, Product };
use Illuminate\Support\Facades\Cache;

function settings(): Setting
{
    return Cache::rememberForever('settings', function () {
        return Setting::firstOrFail();
    });
}

function featuredProducts()
{
    return Cache::rememberForever('featured-products', function () {
        return Product::featured()->limit(6)->get();
    });
}

function pageTitleBySlug(string $slug)
{
    $map = [
        'delivery' => 'Поръчка и доставка',
        'personal_data' => 'Лични данни',
        'terms' => 'Общи условия',
    ];

    return $map[$slug];
}