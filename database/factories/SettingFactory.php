<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'color' => $faker->randomElement(['red', 'green', 'blue', 'orange', 'pink', 'purple']),
        'phone' => $faker->phoneNumber,
        'products_text' => $faker->randomElement(['Продукти', 'Играчки', 'Дрехи', 'Часовници', 'Обувки']),
        'landing_hero_title' => $faker->word,
        'landing_hero_description' => $faker->sentence,
        'landing_hero_c2a' => $faker->word,
        'landing_quality_title' => $faker->word,
        'landing_quality_description' => $faker->sentence,
        'landing_delivery_title' => $faker->word,
        'landing_delivery_description' => $faker->sentence,
        'landing_payment_title' => $faker->word,
        'landing_payment_description' => $faker->sentence,
        'page_delivery' => $faker->sentences(10, true),
        'page_personal_data' => $faker->sentences(10, true),
        'page_terms' => $faker->sentences(10, true),
    ];
});
