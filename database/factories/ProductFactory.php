<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'price' => $faker->randomFloat(2, 1, 59.99),
        'photo' => $faker->word.$faker->fileExtension,
        'is_featured' => $faker->boolean(),
        'description' => $faker->sentences(5, true),
    ];
});
