<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('name');
            $table->string('color');
            $table->string('phone')->nullable();
            $table->string('products_text')->nullable();
            $table->string('landing_hero_title')->nullable();
            $table->string('landing_hero_description')->nullable();
            $table->string('landing_hero_c2a')->nullable();
            $table->string('landing_quality_title')->nullable();
            $table->string('landing_quality_description')->nullable();
            $table->string('landing_delivery_title')->nullable();
            $table->string('landing_delivery_description')->nullable();
            $table->string('landing_payment_title')->nullable();
            $table->string('landing_payment_description')->nullable();
            $table->text('page_delivery')->nullable();
            $table->text('page_personal_data')->nullable();
            $table->text('page_terms')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
