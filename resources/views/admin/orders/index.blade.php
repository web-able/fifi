@extends('layouts.admin', ['title' => 'Поръчки'])

@section('content')
    <livewire:admin.show-orders />
@endsection
