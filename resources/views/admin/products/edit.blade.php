@extends('layouts.admin', ['title' => 'Редакция на '.$product->title])

@section('content')
    <livewire:admin.save-product :product="$product" />
@endsection
