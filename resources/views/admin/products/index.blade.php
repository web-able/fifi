@extends('layouts.admin', ['title' => 'Продукти'])

@section('content')
    <livewire:admin.show-products />
@endsection
