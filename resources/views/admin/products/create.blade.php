@extends('layouts.admin', ['title' => 'Нов Продукт'])

@section('content')
    <livewire:admin.save-product />
@endsection
