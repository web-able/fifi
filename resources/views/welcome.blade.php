<!DOCTYPE html>
<html lang="bg">
    <head>
        @include('common.meta')

        <title>{{ settings()->name }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ mix('css/main.css') }}" rel="stylesheet">
        <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    </head>
    <body
        class="bg-white text-{{ settings()->color }}-700 h-full"
        style="font-family: 'Montserrat', sans-serif;"
    >
        @include('welcome.hero')
        @include('welcome.features')
        @include('welcome.products')

        <x-footer />

        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>
        <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
        <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
        <script>FilePond.registerPlugin(FilePondPluginFileValidateType);</script>
    </body>
</html>
