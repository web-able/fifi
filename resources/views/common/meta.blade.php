@if (app()->isProduction())
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126644583-3"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-126644583-3');
    </script>
@endif

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="description" content="{{ settings()->landing_hero_description }}">
<meta name="keywords" content="{{ settings()->name }}">

<meta property="og:title" content="{{ settings()->landing_hero_title }}">
<meta property="og:description" content="{{ settings()->landing_hero_description }}">
<meta property="og:url" content="{{ config('app.url') }}">
