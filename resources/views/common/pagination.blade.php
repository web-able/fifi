@if ($paginator->hasPages())
    <div class="flex justify-between">
        @unless($paginator->onFirstPage())
            <a
                wire:click="previousPage"
                href="#"
                class="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
            >
                Назад
            </a>
        @endunless

        @if($paginator->hasMorePages())
            <a
                wire:click="nextPage"
                href="#"
                class="ml-auto relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                Напред
            </a>
        @endif
    </div>
@endif
