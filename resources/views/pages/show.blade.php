@extends('layouts.app', ['title' => pageTitleBySlug($slug)])

@section('content')
    <article class="pt-16 leading-tight">
        <h1 class="text-xl font-bold">{{ pageTitleBySlug($slug) }}</h1>

        <p class="mt-4 leading-loose">
            {!! nl2br(settings()->getAttribute('page_'.$slug)) !!}
        </p>
    </article>
@endsection