<div class="bg-{{ settings()->color }}-600">
    <div class="max-w-5xl mx-auto text-center text-gray-200 relative">
        <div class="flex flex-wrap md:-mx-2">
            <x-landing-page-feature
                :title="settings()->landing_quality_title"
                :description="settings()->landing_quality_description"
                class="-mt-10"
            >
                <svg class="w-8 h-8 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 12a6 6 0 1 1 0-12 6 6 0 0 1 0 12zm0-3a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm4 2.75V20l-4-4-4 4v-8.25a6.97 6.97 0 0 0 8 0z"/></svg>
            </x-landing-page-feature>
    
            <x-landing-page-feature
                :title="settings()->landing_delivery_title"
                :description="settings()->landing_delivery_description"
                class="md:-mt-10"
            >
                <svg class="w-8 h-8 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2 14v-3H1a1 1 0 0 1-1-1 1 1 0 0 1 1-1h1l4-7h8l4 7h1a1 1 0 0 1 1 1 1 1 0 0 1-1 1h-1v6a1 1 0 0 1-1 1h-1a1 1 0 0 1-1-1v-1H5v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-3zm13.86-5L13 4H7L4.14 9h11.72zM5.5 14a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm9 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/></svg>
            </x-landing-page-feature>
    
            <x-landing-page-feature
                :title="settings()->landing_payment_title"
                :description="settings()->landing_payment_description"
                class="md:-mt-10"
            >
                <svg class="w-8 h-8 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm1-5h1a3 3 0 0 0 0-6H7.99a1 1 0 0 1 0-2H14V5h-3V3H9v2H8a3 3 0 1 0 0 6h4a1 1 0 1 1 0 2H6v2h3v2h2v-2z"/></svg>
            </x-landing-page-feature>
        </div>
    </div>
</div>
