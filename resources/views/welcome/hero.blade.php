<div class="relative md:h-74vh mb-20 md:mb-0">
    <div class="w-full h-1 bg-{{ settings()->color }}-600"></div>

    <div class="w-11/12 max-w-5xl mx-auto">
        <x-navigation />
    
        <div class="md:flex md:items-center mt-12 md:mt-0 md:h-60vh">
            <div class="md:w-1/2 animated slideInLeft">
                <h1 class="text-3xl uppercase text-{{ settings()->color }}-800 font-bold leading-none">
                    {{ settings()->landing_hero_title }}
                </h1>
                <h2 class="mt-4 text-lg text-{{ settings()->color }}-600">
                    {{ settings()->landing_hero_description }}
                </h2>
                <x:button
                    tag="a"
                    :href="route('products.index')"
                    class="mt-6"
                >
                    {{ settings()->landing_hero_c2a }}
                </x:button>
            </div>

            <div class="hidden md:block w-5/12 ml-auto">
                <img src="/images/hero-illustration-{{ settings()->color }}.svg" />
            </div>
        </div>
    </div>
</div>
