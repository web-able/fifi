@if (featuredProducts()->isNotEmpty())
    <div class="bg-white py-16">
        <h2 class="tracking-widest text-xl uppercase text-center">
            Най-поръчвани продукти
        </h2>

        <div class="w-6 mt-2 mx-auto bg-brand-700 rounded" style="height: 2px"></div>

        <div class="md:max-w-5xl md:mx-auto md:flex md:flex-wrap md:-px-6">
            @foreach (featuredProducts() as $product)
                <div class="w-11/12 mx-auto mt-10 md:w-1/3 md:px-6">
                    <a
                        href="{{ route('products.show', $product) }}"
                        class="block relative bg-white overflow-hidden shadow-lg rounded-lg"
                    >
                        <img
                            src="{{ Storage::url($product->photo) }}"
                            class="w-full h-32 object-cover"
                        />

                        <div
                            class="absolute top-0 left-0 opacity-50 w-full h-full rounded-lg z-10"
                            style="background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,1));"
                        ></div>
                        
                        <div class="absolute bottom-0 left-0 pb-2 pl-3 text-gray-100 z-20 text-bold">
                            {{ $product->name }}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endif