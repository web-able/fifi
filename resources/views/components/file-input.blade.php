<div
    {{ $attributes }}
    wire:ignore
    x-data
    x-init="
        FilePond
            .create($refs.input)
            .setOptions({
                allowReplace: false,
                server: {
                    process: {
                        url: '{{ route('admin.photos.store') }}',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        onload: function(filename) {
                            $dispatch('input', filename)
                        }
                    }
                },
                labelIdle: 'Качете снимка като натисните или провлачите тук',
                labelFileLoading: 'Зарежда...',
                labelFileProcessing: 'Качва се...',
                labelFileProcessingComplete: 'Успешно качено',
                labelFileProcessingAborted: 'Качването беше спряно',
                labelFileProcessingError: 'Възнинка грешка при качването',
                labelTapToCancel: 'Натиснете, за да спрете',
                labelTapToRetry: 'Натиснете, за да опитате отново',
                labelTapToUndo: 'Натиснете, за да върнете',
                labelButtonRemoveItem: 'Премахнете',
                labelButtonAbortItemLoad: 'Спрете',
                labelButtonRetryItemLoad: 'Наново',
                labelButtonAbortItemProcessing: 'Откажете',
            });
    "
>
    <input
        x-ref="input"
        type="file"
        accept="image/*"
    />
</div>
