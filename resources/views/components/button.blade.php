@props(['tag'])

<{{ $tag }}
    @if($tag === 'button') type="submit" @endif
    {{ $attributes->merge(['class' => 'text-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white tracking-wider bg-'.settings()->color.'-600 disabled:bg-'.settings()->color.'-300 hover:bg-'.settings()->color.'-800 focus:outline-none focus:border-'.settings()->color.'-900 focus:shadow-outline-red active:bg-'.settings()->color.'-900 transition ease-in-out duration-300' . ($tag === 'a' ? ' inline-block' : '')]) }}>
    {{ $slot }}
</{{ $tag }}>
