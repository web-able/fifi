<footer {{ $attributes->merge(['class' => 'mt-10']) }}">
    <div class="max-w-screen-xl mx-auto py-12 px-4 overflow-hidden sm:px-6 lg:px-8">
        <nav class="-mx-5 -my-2 flex flex-wrap justify-center">
            <div class="px-5 py-2">
                <a
                    href="{{ route('pages.show', 'delivery') }}"
                    class="text-base leading-6 text-gray-500 hover:text-gray-900"
                >
                    Поръчка и доставка
                </a>
            </div>
            <div class="px-5 py-2">
                <a
                    href="{{ route('pages.show', 'personal_data') }}"
                    class="text-base leading-6 text-gray-500 hover:text-gray-900"
                >
                    Лични данни
                </a>
            </div>
            <div class="px-5 py-2">
                <a
                    href="{{ route('pages.show', 'terms') }}"
                    class="text-base leading-6 text-gray-500 hover:text-gray-900"
                >
                    Общи условия
                </a>
            </div>
        </nav>
        <div class="mt-8">
            <p class="text-center text-base leading-6 text-gray-400">
                &copy; {{ date('Y').' '.settings()->name }} - всички права запазени.
            </p>
        </div>
    </div>
</footer>
