@props(['title', 'description'])

<div class="w-11/12 md:w-1/3 mx-auto md:px-2 animated slideInUp">
    <div {{ $attributes->merge(['class' => 'w-24 h-24 mx-auto bg-'.settings()->color.'-600 flex justify-center items-center rounded-full']) }}>
        <div class="w-20 h-20 border border-gray-200 flex justify-center items-center rounded-full">
            {{ $slot }}
        </div>
    </div>

    <div class="mt-2 text-gray-200 uppercase tracking-wider">
        {{ $title }}
    </div>

    <div class="mt-2 mb-12 text-{{ settings()->color }}-200">
        {{ $description }}
    </div>
</div>
