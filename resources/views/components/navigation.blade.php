<div
    x-data="{ showMenu: false }"
    class="flex justify-between items-center pt-10"
>
    <a href="/" class="tracking-tighter text-xl font-bold text-{{ settings()->color }}-600">
        {{ settings()->name }}
    </a>

    <nav class="hidden md:flex tracking-wider uppercase text-sm font-bold text-{{ settings()->color }}-600">
        <a href="{{ route('products.index') }}" class="ml-6">
            {{ settings()->products_text }}
        </a>
    </nav>

    <a
        @click.prevent="showMenu = true"
        href="#"
        class="block md:hidden ml-2 h-5 w-5"
    >
        <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </a>

    <div
        x-show="showMenu"
        class="absolute z-50 top-0 inset-x-0 mx-2 py-2 md:hidden"
        style="display: none"
        @click.away="showMenu = false"
    >
        <div class="rounded-lg shadow-md transition transform origin-top-right" x-show="showMenu" x-transition:enter="duration-150 ease-out" x-transition:enter-start="opacity-0 scale-95" x-transition:enter-end="opacity-100 scale-100" x-transition:leave="duration-100 ease-in" x-transition:leave-start="opacity-100 scale-100" x-transition:leave-end="opacity-0 scale-95">
            <div class="rounded-lg bg-white shadow-xs overflow-hidden">
                <div class="px-5 pt-4 flex items-center justify-between">
                    <a href="/">
                        {{ settings()->name }}
                    </a>
                    <div class="-mr-2">
                        <button @click.prevent="showMenu = false" type="button" class="inline-flex items-center justify-center p-2 rounded-md text-{{ settings()->color }}-400 hover:text-{{ settings()->color }}-600 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-{{ settings()->color }}-600 transition duration-150 ease-in-out">
                            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="px-2 pt-6 pb-3">
                    <a
                        href="{{ route('products.index') }}"
                        class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-{{ settings()->color }}-700 hover:text-{{ settings()->color }}-900 hover:bg-gray-50 focus:outline-none focus:text-{{ settings()->color }}-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                    >{{ settings()->products_text }}</a>
                </div>
                <div>
                    <a href="tel:{{ config('app.phone.raw') }}" class="block w-full px-5 py-3 text-center font-medium text-{{ settings()->color }}-600 bg-gray-50 hover:bg-gray-100 hover:text-{{ settings()->color }}-700 focus:outline-none focus:bg-gray-100 focus:text-{{ settings()->color }}-700 transition duration-150 ease-in-out">
                        {{ config('app.phone.readable') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>