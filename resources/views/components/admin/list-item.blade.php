<div {{ $attributes }} x-data="{ open: false }">
    <div @click="open = !open" class="cursor-pointer flex items-center">
        <div class="font-bold">
            {{ $title }}
        </div>
        <div class="ml-1 w-4 h-4">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
        </div>
    </div>

    <div x-show="open" class="text-gray-600">
        {{ $description }}
    </div>
</div>