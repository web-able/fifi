@extends('layouts.app', ['title' => $product->title])

@section('content')
    <livewire:show-product :product="$product" />
@endsection
