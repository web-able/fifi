@extends('layouts.app', ['title' => settings()->products_text])

@section('content')
    <livewire:show-products />
@endsection
