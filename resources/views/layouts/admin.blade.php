<!DOCTYPE html>
<html lang="bg">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Admin</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

        @livewireStyles

        <link href="{{ mix('css/main.css') }}" rel="stylesheet">
        <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    </head>
    <body class="bg-white text-gray-700" style="font-family: 'Montserrat', sans-serif;">
        <div
            class="h-screen flex overflow-hidden bg-gray-100"
            x-data="{ sidebarOpen: false }"
            @keydown.window.escape="sidebarOpen = false"
        >
            <!-- Off-canvas menu for mobile -->
            <div x-show="sidebarOpen" class="md:hidden">
                <div @click="sidebarOpen = false" x-show="sidebarOpen" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0 z-30 transition-opacity ease-linear duration-300">
                    <div class="absolute inset-0 bg-gray-600 opacity-75"></div>
                </div>
                <div class="fixed inset-0 flex z-40">
                    <div x-show="sidebarOpen" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" class="flex-1 flex flex-col max-w-xs w-full bg-{{ settings()->color }}-800 transform ease-in-out duration-300 ">
                        <div class="absolute top-0 right-0 -mr-14 p-1">
                            <button x-show="sidebarOpen" @click="sidebarOpen = false" class="flex items-center justify-center h-12 w-12 rounded-full focus:outline-none focus:bg-gray-600">
                                <svg class="h-6 w-6 text-white" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>

                        <div class="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
                            <div class="flex-shrink-0 flex items-center px-4">
                                <span class="text-{{ settings()->color}}-100 font-bold text-xl tracking-tight">{{ settings()->name }}</span>
                            </div>
                            <nav class="mt-5 px-2">
                                <a
                                    href="{{ route('admin.dashboard') }}"
                                    class="group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md {{ Request::is('admin') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white' }} focus:outline-none focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                                >
                                    <x:heroicon-o-view-boards class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                    Табло
                                </a>

                                <a
                                    href="{{ route('admin.orders.index') }}"
                                    class="mt-1 group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md {{ Request::is('admin/orders*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                                >
                                    <x:heroicon-o-shopping-cart class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                    Поръчки
                                </a>

                                <a
                                    href="{{ route('admin.products.index') }}"
                                    class="mt-1 group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md {{ Request::is('admin/products*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                                >
                                    <x:heroicon-o-sun class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                    {{ settings()->products_text }}
                                </a>

                                <a
                                    href="{{ route('admin.settings') }}"
                                    class="mt-1 group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md {{ Request::is('admin/products*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                                >
                                    <x:heroicon-o-adjustments class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                    Настройки
                                </a>

                                <a
                                    href="{{ route('admin.texts') }}"
                                    class="mt-1 group flex items-center px-2 py-2 text-base leading-6 font-medium rounded-md {{ Request::is('admin/products*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                                >
                                    <x:heroicon-o-translate class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                    Текстове
                                </a>
                            </nav>
                        </div>

                        <div class="flex-shrink-0 flex border-t border-{{ settings()->color }}-700 p-4">
                            <a href="#" class="flex-shrink-0 group block focus:outline-none">
                                <div class="flex items-center">
                                    <livewire:logout />
                                </div>
                            </a>   
                        </div>
                    </div>
                    <div class="flex-shrink-0 w-14">
                        <!-- Force sidebar to shrink to fit close icon -->
                    </div>
                </div>
            </div>

            <!-- Static sidebar for desktop -->
            <div class="hidden md:flex md:flex-shrink-0">
                <div class="flex flex-col w-64 border-r border-gray-200 bg-{{ settings()->color }}-800">
                    <div class="h-0 flex-1 flex flex-col pt-5 pb-4 overflow-y-auto">
                        <div class="flex items-center flex-shrink-0 px-4">
                        <span class="text-{{ settings()->color}}-100 font-bold text-xl tracking-tight">{{ settings()->name }}</span>
                        </div>

                        <nav class="mt-5 flex-1 px-2 bg-{{ settings()->color }}-800">
                            <a
                                href="{{ route('admin.dashboard') }}"
                                class="group flex items-center px-2 py-2 text-sm leading-5 font-medium rounded-md {{ Request::is('admin') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white hover:bg-'.settings()->color.'-700' }} focus:outline-none focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                            >
                                <x:heroicon-o-view-boards class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                Табло
                            </a>
                            <a
                                href="{{ route('admin.orders.index') }}"
                                class="mt-1 group flex items-center px-2 py-2 text-sm leading-5 font-medium rounded-md {{ Request::is('admin/orders*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white hover:bg-'.settings()->color.'-700' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                            >
                                <x:heroicon-o-shopping-cart class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                Поръчки
                            </a>
                            <a
                                href="{{ route('admin.products.index') }}"
                                class="mt-1 group flex items-center px-2 py-2 text-sm leading-5 font-medium rounded-md {{ Request::is('admin/products*') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white hover:bg-'.settings()->color.'-700' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                            >
                                <x:heroicon-o-sun class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                {{ settings()->products_text }}
                            </a>
                            <a
                                href="{{ route('admin.settings') }}"
                                class="mt-1 group flex items-center px-2 py-2 text-sm leading-5 font-medium rounded-md {{ Request::is('admin/settings') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white hover:bg-'.settings()->color.'-700' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                            >
                                <x:heroicon-o-adjustments class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                Настройки
                            </a>
                            <a
                                href="{{ route('admin.texts') }}"
                                class="mt-1 group flex items-center px-2 py-2 text-sm leading-5 font-medium rounded-md {{ Request::is('admin/texts') ? 'text-white bg-'.settings()->color.'-900' : 'text-'.settings()->color.'-300 hover:text-white hover:bg-'.settings()->color.'-700' }} focus:outline-none focus:text-white focus:bg-{{ settings()->color }}-700 transition ease-in-out duration-150"
                            >
                                <x:heroicon-o-translate class="mr-3 h-6 w-6 text-{{ settings()->color }}-400 group-hover:text-{{ settings()->color }}-300 group-focus:text-{{ settings()->color }}-300 transition ease-in-out duration-150" />
                                Текстове
                            </a>
                        </nav>
                    </div>
                    <div class="p-4">
                        <a href="/" class="text-{{ settings()->color }}-300">
                            Към сайта
                        </a>
                    </div>
                    <div class="border-t border-{{ settings()->color }}-700 p-4">
                        <livewire:logout />
                    </div>
                </div>
            </div>

            <div class="flex flex-col w-0 flex-1 overflow-hidden">
                <div class="md:hidden pl-1 pt-1 sm:pl-3 sm:pt-3">
                    <button @click.stop="sidebarOpen = true" class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:bg-gray-200 transition ease-in-out duration-150">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                        </svg>
                    </button>
                </div>
                <main class="flex-1 relative z-0 overflow-y-auto pt-2 pb-6 focus:outline-none md:py-6" tabindex="0" x-data x-init="$el.focus()">
                    <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                        <h1 class="text-2xl font-semibold text-gray-900">{{ $title }}</h1>
                    </div>

                    <div class="max-w-7xl mx-auto px-4 py-6 sm:px-6 md:px-8">
                        @yield('content')
                    </div>
                </main>
            </div>
        </div>

        @livewireScripts
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>
        <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
        <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
        <script>FilePond.registerPlugin(FilePondPluginFileValidateType);</script>
    </body>
</html>
