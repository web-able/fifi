<!DOCTYPE html>
<html lang="bg">
    <head>
        @include('common.meta')

        <title>{{ $title }} | {{ settings()->name }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

        @livewireStyles

        <link href="{{ mix('css/main.css') }}" rel="stylesheet">
    </head>
    <body class="bg-white text-gray-700" style="font-family: 'Montserrat', sans-serif;">
        <div class="w-full h-1 bg-{{ settings()->color }}-600"></div>

        <div class="w-11/12 max-w-5xl mx-auto">
            <x-navigation class="py-4 text-white" />
        </div>

        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="max-w-3xl mx-auto">
                @yield('content')
            </div>
        </div>

        <x-footer/>

        @livewireScripts
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
