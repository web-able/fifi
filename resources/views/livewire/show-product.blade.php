<div>
    @if ($success)
        <h1 class="mt-10 mb-4 text-lg font-bold text-gray-900 text-center">
            Успешна Поръчка
        </h1>

        <p class="w-11/12 md:w-2/3 mx-auto mb-8 text-gray-600 text-center">
            Благодарим Ви за поръчката! Очаквайте да се свържем с Вас за потвърждение чрез посоченият от Вас телефон!
        </p>

        <div class="w-11/12 md:w-1/2 mx-auto">
            @include('common.illustrations.success')
        </div>

        <div class="mt-16 text-center">
            <div class="font-bold text-lg">Желаете ли да поръчате нещо друго?</div>

            <div class="mt-6">
                <x-button
                    tag="a"
                    :href="route('products.index')"
                >
                    Обратно към магазина
                </x-button>
            </div>
        </div>
    @else
        <div>
            <img
                src="{{ Storage::url($product->photo) }}"
                class="w-full mt-10 h-48 md:h-72 object-cover shadow-lg rounded-lg"
            />

            <div class="flex items-center mt-10 text-gray-900">
                <div class="text-bold">
                    {{ $product->title }}
                </div>
                <div class="mx-2">·</div>
                <div class="text-gray-900">
                    {{ $product->price }}лв
                </div>
            </div>

            <div class="mt-4 text-gray-600">
                {{ $product->description }}
            </div>
        </div>

        <form wire:submit.prevent="order" class="block mt-6">
            <div class="md:w-1/2">
                <label for="phone" class="sr-only">Телефон</label>
                <div class="relative">
                    <input
                        wire:model="phone"
                        id="phone"
                        class="form-input block w-full sm:text-sm sm:leading-5 @error('phone') text-{{ settings()->color }}-600 focus:shadow-outline-red @enderror"
                        placeholder="Телефон"
                    />

                    @error('phone')
                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                            <svg class="h-5 w-5 text-{{ settings()->color }}-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                    @enderror
                </div>
    
                @error('phone')
                    <p class="mt-1 text-sm text-{{ settings()->color }}-600">Непълен телефонен номер.</p>
                @enderror
            </div>

            <div class="md:w-1/2 mt-4">
                <label for="address" class="sr-only">Адрес</label>
                <div class="relative">
                    <textarea
                        wire:model="address"
                        id="address"
                        class="form-input block w-full sm:text-sm sm:leading-5 @error('address') text-{{ settings()->color }}-600 focus:shadow-outline-red @enderror"
                        placeholder="Адрес за Доставка"
                        rows="4"
                    ></textarea>

                    @error('address')
                        <div class="absolute inset-y-0 right-0 pr-3 pt-2 flex pointer-events-none">
                            <svg class="h-5 w-5 text-{{ settings()->color }}-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                    @enderror
                </div>
    
                @error('address')
                    <p class="mt-1 text-sm text-{{ settings()->color }}-600">Непълен адрес.</p>
                @enderror
            </div>

            <div class="md:w-1/2 mt-4">
                <label for="text" class="sr-only">Допълнителна Информация</label>
                <div class="relative">
                    <textarea
                        wire:model="additions"
                        id="text"
                        class="form-input block w-full sm:text-sm sm:leading-5 @error('text') text-{{ settings()->color }}-600 focus:shadow-outline-red @enderror"
                        placeholder="Допълнителна Информация"
                        rows="2"
                    ></textarea>

                    @error('text')
                        <div class="absolute inset-y-0 right-0 pr-3 pt-2 flex pointer-events-none">
                            <svg class="h-5 w-5 text-{{ settings()->color }}-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                            </svg>
                        </div>
                    @enderror
                </div>
    
                @error('text')
                    <p class="mt-1 text-sm text-{{ settings()->color }}-600">Невалиден текст.</p>
                @enderror
            </div>
        
            <div class="mt-4">
                <x-button tag="button">
                    Поръчай
                </x-button>
            </div>
        </form>
    @endif
</div>