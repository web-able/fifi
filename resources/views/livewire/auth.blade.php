<form wire:submit.prevent="auth" class="block mt-10 md:w-3/5 md:mx-auto md:px-4">
    <div>
        <div>
            <label for="email" class="sr-only">Email</label>
            <div class="relative">
                <input
                    wire:model="email"
                    id="email"
                    class="form-input block w-full sm:text-sm sm:leading-5 @if($hasErrors) text-{{ settings()->color }}-600 focus:shadow-outline-red @endif"
                    placeholder="Email"
                />

                @if($hasErrors)
                    <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                        <svg class="h-5 w-5 text-{{ settings()->color }}-500" fill="currentColor" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                        </svg>
                    </div>
                @endif
            </div>

            @if($hasErrors)
                <p class="mt-1 text-sm text-{{ settings()->color }}-600">Грешни данни за вход (може и паролата да е грешна).</p>
            @endif
        </div>
        
        <div class="mt-4">
            <label for="password" class="sr-only">Парола</label>
            <div class="relative">
                <input
                    wire:model="password"
                    type="password"
                    id="password"
                    class="form-input block w-full sm:text-sm sm:leading-5 @if($hasErrors) text-{{ settings()->color }}-600 focus:shadow-outline-red @endif"
                    placeholder="Парола"
                />
            </div>
        </div>
    </div>

    <div class="mt-4">
        <button
            type="submit"
            class="w-full text-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-{{ settings()->color }}-500 hover:bg-{{ settings()->color }}-400 focus:outline-none focus:border-{{ settings()->color }}-600 focus:shadow-outline-red active:bg-{{ settings()->color }}-500 transition ease-in-out duration-150"
        >
            Вход
        </button>
    </div>
</form>
