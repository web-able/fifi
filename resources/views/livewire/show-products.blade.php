<div>
    @if ($products->count() == 0)
        <div class="mt-10 flex flex-col items-center">
            <div class="font-bold text-xl">Очаквайте скоро нашите продукти</div>

            <div class="mt-10 w-64">
                @include('common.illustrations.empty')
            </div>
        </div>
    @else
        <div class="flex flex-wrap md:-mx-6">
            @foreach ($products as $product)
                <div class="w-full mt-10 md:w-1/3 md:px-6">
                    <a
                        href="{{ route('products.show', $product) }}"
                        class="block relative bg-white overflow-hidden shadow-lg rounded-lg"
                    >
                        <img
                            src="{{ Storage::url($product->photo) }}"
                            class="w-full h-64 object-cover"
                        />

                        <div
                            class="absolute top-0 left-0 opacity-50 w-full h-full rounded-lg z-10"
                            style="background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,1));"
                        ></div>
                        
                        <div class="absolute bottom-0 left-0 pb-2 pl-3 text-gray-100 z-20 text-bold">
                            {{ $product->title }}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

        <div class="mt-10">
            {{ $products->links('common.pagination') }}
        </div>
    @endif
</div>
