<form wire:submit.prevent="save">
    <div><!-- Products Text -->
        <label for="products_text">Какви са Вашите продукти? Например: Дрехи</label>
        <input
            wire:model.lazy="products_text"
            id="products_text"
            class="form-input block w-full md:w-1/5 sm:text-sm sm:leading-5"
        />
        @error('products_text') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Title -->
        <label for="landing_hero_title">Заглавие в началната страница</label>
        <input
            wire:model.lazy="landing_hero_title"
            id="landing_hero_title"
            class="form-input block w-full md:w-1/3 sm:text-sm sm:leading-5"
        />
        @error('landing_hero_title') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Description -->
        <label for="landing_hero_description">Кратко изречение в началната страница</label>
        <input
            wire:model.lazy="landing_hero_description"
            id="landing_hero_description"
            class="form-input block w-full md:w-2/3 sm:text-sm sm:leading-5"
        />
        @error('landing_hero_description') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page C2A -->
        <label for="landing_hero_c2a">Бутон в началната страница</label>
        <input
            wire:model.lazy="landing_hero_c2a"
            id="landing_hero_c2a"
            class="form-input block w-full md:w-1/3 sm:text-sm sm:leading-5"
        />
        @error('landing_hero_c2a') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Quality Title -->
        <label for="landing_quality_title">Представяне #1 Заглавие</label>
        <input
            wire:model.lazy="landing_quality_title"
            id="landing_quality_title"
            class="form-input block w-full md:w-1/5 sm:text-sm sm:leading-5"
        />
        @error('landing_quality_title') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Quality Description -->
        <label for="landing_quality_description">Представяне #1 Описание</label>
        <input
            wire:model.lazy="landing_quality_description"
            id="landing_quality_description"
            class="form-input block w-full sm:text-sm sm:leading-5"
        />
        @error('landing_quality_description') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Delivery Title -->
        <label for="landing_delivery_title">Представяне #2 Заглавие</label>
        <input
            wire:model.lazy="landing_delivery_title"
            id="landing_delivery_title"
            class="form-input block w-full md:w-1/5 sm:text-sm sm:leading-5"
        />
        @error('landing_delivery_title') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Delivery Description -->
        <label for="landing_delivery_description">Представяне #2 Описание</label>
        <input
            wire:model.lazy="landing_delivery_description"
            id="landing_delivery_description"
            class="form-input block w-full sm:text-sm sm:leading-5"
        />
        @error('landing_delivery_description') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page Payment Title -->
        <label for="landing_payment_title">Представяне #3 Заглавие</label>
        <input
            wire:model.lazy="landing_payment_title"
            id="landing_payment_title"
            class="form-input block w-full md:w-1/5 sm:text-sm sm:leading-5"
        />
        @error('landing_payment_title') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Landing Page payment Description -->
        <label for="landing_payment_description">Представяне #3 Описание</label>
        <input
            wire:model.lazy="landing_payment_description"
            id="landing_payment_description"
            class="form-input block w-full sm:text-sm sm:leading-5"
        />
        @error('landing_payment_description') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Page Delivery -->
        <label for="page_delivery">Страница "Поръчка и доставка"</label>
        <textarea
            wire:model.lazy="page_delivery"
            id="page_delivery"
            class="form-input block w-full md:w-2/3 sm:text-sm sm:leading-5"
            rows="10"
        ></textarea>
        @error('page_delivery') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Page Personal -->
        <label for="page_personal_data">Страница "Лични данни"</label>
        <textarea
            wire:model.lazy="page_personal_data"
            id="page_personal_data"
            class="form-input block w-full md:w-2/3 sm:text-sm sm:leading-5"
            rows="10"
        ></textarea>
        @error('page_personal_data') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Page Terms -->
        <label for="page_terms">Страница "Общи условия"</label>
        <textarea
            wire:model.lazy="page_terms"
            id="page_terms"
            class="form-input block w-full md:w-2/3 sm:text-sm sm:leading-5"
            rows="10"
        ></textarea>
        @error('page_terms') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <x:button tag="button">
            Запази
        </x:button>
    </div>
</form>
