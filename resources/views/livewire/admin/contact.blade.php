<div class="mt-6">
    @if ($success)
        <div class="bg-blue-200 text-blue-700 rounded p-4">
            Получихме Вашето запитване, ще Ви отговорим при първа възможност на посочения от Вас мейл: {{ $email }}
        </div>
    @else
        <form wire:submit.prevent="send">
            <div>
                <label for="title">Тема</label>
                <input
                    wire:model.lazy="title"
                    id="title"
                    class="form-input block w-full sm:text-sm sm:leading-5"
                    placeholder="пример: Как да сменя цвета на сайта?"
                />
                @error('title') <div class="text-red-600">{{ $message }}</div> @enderror
            </div>

            <div class="mt-6">
                <label for="description">Описание</label>
                <textarea
                    wire:model.lazy="description"
                    id="description"
                    class="form-input block w-full sm:text-sm sm:leading-5"
                    placeholder="пример: Не мога да се ориентирам къде да вляза, за да..."
                    rows="6"
                ></textarea>
                @error('description') <div class="text-red-600">{{ $message }}</div> @enderror
            </div>

            <div class="mt-6">
                <x:button tag="button">
                    Изпрати
                </x:button>
            </div>
        </form>
    @endif
</div>
