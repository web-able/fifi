<form wire:submit.prevent="save">
    @if ($productId)
        <img src="{{ Storage::url($photo) }}" class="w-64" />
    @endif

    <div class="mt-6 w-full lg:w-1/3">
        <x:file-input wire:model="photo" />
        @error('photo') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <label for="title">Име</label>
        <input
            wire:model.lazy="title"
            id="title"
            class="form-input block w-full md:w-1/3 sm:text-sm sm:leading-5"
            placeholder="Име"
        />
        @error('title') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <label for="price">Цена</label>
        <input
            wire:model.lazy="price"
            id="price"
            class="form-input block w-full md:w-1/3 sm:text-sm sm:leading-5"
            placeholder="Цена"
        />
        @error('price') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <label for="description">Описание</label>
        <textarea
            wire:model.lazy="description"
            id="description"
            class="form-input block w-full md:w-2/3 sm:text-sm sm:leading-5"
            placeholder="Описание"
            rows="6"
        ></textarea>
        @error('description') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <input
            wire:model.lazy="isFeatured"
            type="checkbox"
            id="isFeatured"
        />

        <label for="isFeatured">На заглавна страница?</label>
        @error('isFeatured') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6">
        <x:button tag="button">
            Запази
        </x:button>
    </div>
</form>
