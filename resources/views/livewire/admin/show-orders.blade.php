<div wire:poll.30s="refreshOrders">
    @if ($orders->count() == 0)
        <div class="max-w-xl">
            Към момента няма направени поръчки през сайта. Но когато има, те ще се появяват тук. За да се уверите, че всичко работи, Ви съветваме Вие самите да си пуснете една тестова поръчка.
        </div>

        <div class="mt-10 lg:w-1/3">
            @include('common.illustrations.empty')
        </div>
    @else
        {{-- Disabled for now
        <div class="rounded bg-gray-200 text-gray-600 p-4">
            На всеки 30 секунди страницата се обновява *автоматично, няма нужда да презареждате.
            <div class="mt-1 text-gray-500">
                * Необходимо е таба да е отворен и активен.
            </div>
        </div> --}}

        <div class="mt-6 flex flex-col">
            <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <table class="min-w-full">
                    <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Продукт
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                За
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Статус
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Създадена
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Опции
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach($orders as $order)
                            <tr>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="flex items-center">
                                        <img
                                            src="{{ Storage::url($order->product->photo) }}"
                                            class="w-8 h-8 object-cover rounded-full"
                                        />
                                        <div class="ml-6">
                                            <div class="text-sm leading-5 font-medium text-gray-900">
                                                {{ $order->product->title }}
                                            </div>
                                            <div class="text-xl leading-5 font-medium text-gray-900">
                                                {{ $order->product->price }}<span class="text-sm">лв</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 text-gray-900">
                                        {{ $order->phone }}
                                    </div>
                                    <div class="text-sm leading-5 text-gray-500 truncate w-64">
                                        {{ $order->address }}
                                    </div>
                                    <div class="text-sm leading-5 text-gray-400 truncate w-64">
                                        {{ $order->additions }}
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full @if($order->status === 'Изпратена') bg-green-100 text-green-800 @else bg-yellow-100 text-yellow-800 @endif">
                                        {{ $order->status }}
                                    </span>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    {{ $order->created_at->diffForHumans() }}
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    <div x-data="{ open: false }" class="relative">
                                        <a
                                            x-show="! open"
                                            @click.prevent="open = true"
                                            href="#"
                                            class="text-3xl tracking-widest font-bold"
                                        >...</a>
                                
                                        <ul
                                            x-show="open"
                                            @click.away="open = false"
                                            class="z-10 absolute bottom-0 right-0 rounded shadow-xl bg-gray-100 py-4 px-6"
                                        >
                                            @if ($order->status === 'Обработва се')
                                                <li class="pb-4">
                                                    <a
                                                        @click="open = false"
                                                        wire:click.prevent="changeStatus({{ $order->id }}, 'Изпратена')"
                                                        href="#"
                                                    >
                                                        Смени статус на Изпратена
                                                    </a>
                                                </li>
                                            @endif

                                            @if ($order->status === 'Изпратена')
                                                <li class="pb-4">
                                                    <a
                                                        @click="open = false"
                                                        wire:click.prevent="changeStatus({{ $order->id }}, 'Обработва се')"
                                                        href="#"
                                                    >
                                                        Смени статус на Обработва се
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>
