<form wire:submit.prevent="save">
    <div><!-- Name -->
        <label for="name">Име</label>
        <input
            wire:model.lazy="name"
            id="name"
            class="form-input block w-full md:w-1/3 sm:text-sm sm:leading-5"
            placeholder="Име"
        />
        @error('name') <div class="text-red-600">{{ $message }}</div> @enderror
    </div>

    <div class="mt-6"><!-- Color -->
        <label for="name">Цвят</label>

        <div class="flex items-center mt-3 md:text-3xl">
            <input
                wire:model="color"
                type="radio"
                id="color-red"
                value="red"
                class="hidden"
            />
            <label
                for="color-red"
                class="block w-8 h-8 md:w-12 md:h-12 bg-red-600 rounded-full shadow {{ $color == 'red' ? 'transform scale-125 shadow-inner border-4 border-red-900 shadow-xl' : 'border border-red-500' }}"
            ></label>
            
            <input
                wire:model="color"
                type="radio"
                id="color-green"
                value="green"
                class="hidden"
            />
            <label
                for="color-green"
                class="ml-3 block w-8 h-8 md:w-12 md:h-12 bg-green-600 rounded-full shadow {{ $color == 'green' ? 'transform scale-125 shadow-inner border-4 border-green-900 shadow-xl' : 'border border-green-500' }}"
            ></label>
            
            <input
                wire:model="color"
                type="radio"
                id="color-blue"
                value="blue"
                class="hidden"
            />
            <label
                for="color-blue"
                class="ml-3 block w-8 h-8 md:w-12 md:h-12 bg-blue-600 rounded-full shadow {{ $color == 'blue' ? 'transform scale-125 shadow-inner border-4 border-blue-900 shadow-xl' : 'border border-blue-500' }}"
            ></label>
            
            <input
                wire:model="color"
                type="radio"
                id="color-orange"
                value="orange"
                class="hidden"
            />
            <label
                for="color-orange"
                class="ml-3 block w-8 h-8 md:w-12 md:h-12 bg-orange-600 rounded-full shadow {{ $color == 'orange' ? 'transform scale-125 shadow-inner border-4 border-orange-900 shadow-xl' : 'border border-orange-500' }}"
            ></label>
            
            <input
                wire:model="color"
                type="radio"
                id="color-purple"
                value="purple"
                class="hidden"
            />
            <label
                for="color-purple"
                class="ml-3 block w-8 h-8 md:w-12 md:h-12 bg-purple-600 rounded-full shadow {{ $color == 'purple' ? 'transform scale-125 shadow-inner border-4 border-purple-900 shadow-xl' : 'border border-purple-500' }}"
            ></label>
            
            <input
                wire:model="color"
                type="radio"
                id="color-pink"
                value="pink"
                class="hidden"
            />
            <label
                for="color-pink"
                class="ml-3 block w-8 h-8 md:w-12 md:h-12 bg-pink-600 rounded-full shadow {{ $color == 'pink' ? 'transform scale-125 shadow-inner border-4 border-pink-900 shadow-xl' : 'border border-pink-500' }}"
            ></label>
        </div>
    
        @error('color')
            <div class="mt-1 text-red-400 text-sm">{{ $message }}</div>
        @enderror
    </div>

    <div class="mt-10">
        <x:button tag="button">
            Запази
        </x:button>
    </div>
</form>
