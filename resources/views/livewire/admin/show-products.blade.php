<div>
    @if ($hasDeletionFailed)
        <div class="mb-6 bg-red-500 text-red-100 rounded p-6">
            Изтриването не е възможно, тъй като съществуват поръчки с този продукт
        </div>
    @endif

    @if ($products->count() == 0)
        <div class="mt-10 lg:w-1/3">
            <h1 class="font-bold text-xl">
                Няма добавени продукти
            </h1>

            <div class="mt-10">
                @include('common.illustrations.empty')
            </div>

            <x-button
                tag="a"
                :href="route('admin.products.create')"
                class="mt-10 shadow"
            >
                Добавете Първия Продукт
            </x-button>
        </div>
    @else
        <x-button
            tag="a"
            :href="route('admin.products.create')"
            class="shadow"
        >
            Нов Продукт
        </x-button>

        <div class="flex flex-col mt-6">
            <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                    <table class="min-w-full">
                    <thead>
                        <tr>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Продукт
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Описание
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                На заглавна страница?
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Създадена
                            </th>
                            <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                Опции
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white">
                        @foreach($products as $product)
                            <tr>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="flex items-center">
                                        <img
                                            src="{{ Storage::url($product->photo) }}"
                                            class="w-8 h-8 object-cover rounded-full"
                                        />
                                        <div class="ml-6">
                                            <div class="text-sm leading-5 font-medium text-gray-900">
                                                {{ $product->title }}
                                            </div>
                                            <div class="text-xl leading-5 font-medium text-gray-900">
                                                {{ $product->price }}<span class="text-sm">лв</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <div class="text-sm leading-5 text-gray-900 truncate w-64">
                                        {{ $product->description }}
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full @if($product->is_featured) bg-green-100 text-green-800 @else bg-red-100 text-red-800 @endif">
                                        @if($product->is_featured) Да @else Не @endif
                                    </span>
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    {{ $product->created_at->diffForHumans() }}
                                </td>
                                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                                    <div x-data="{ open: false }" class="relative">
                                        <a
                                            x-show="! open"
                                            @click.prevent="open = true"
                                            href="#"
                                            class="text-3xl tracking-widest font-bold"
                                        >...</a>
                                
                                        <ul
                                            x-show="open"
                                            @click.away="open = false"
                                            class="z-10 absolute bottom-0 right-0 rounded shadow-xl bg-gray-100 py-4 px-6"
                                        >
                                            <li class="pb-4">
                                                <a href="{{ route('admin.products.edit', $product) }}">
                                                    Редактирай
                                                </a>
                                            </li>
                                            <li>
                                                <a
                                                    wire:click.prevent="delete({{ $product->id }})"
                                                    @click.prevent="open = false"
                                                    href="#"
                                                >
                                                    Изтрий
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>